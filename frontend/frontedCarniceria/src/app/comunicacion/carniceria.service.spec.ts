import { TestBed } from '@angular/core/testing';

import { CarniceriaService } from './carniceria.service';

describe('CarniceriaService', () => {
  let service: CarniceriaService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(CarniceriaService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
