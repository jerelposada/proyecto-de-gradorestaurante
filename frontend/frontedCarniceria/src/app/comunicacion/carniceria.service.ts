import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class CarniceriaService {
  API= 'http://localhost:3005/';
  constructor( private http : HttpClient) {
  }

  getAllCarniceria(){
    return this.http.get(this.API);
  }

  saveCarne( nombre : string , precio : string, unidadMedida: string,cantidad: string,TipoCarniceriaId){
    return this.http.post(this.API,{
      nombre, precio,unidadMedida,cantidad,TipoCarniceriaId
    });
  }

  getCarneByID(id :string){
    return this.http.get(`${this.API}${id}`);

  }

  UpdateCarneById(id :string, cantidad : String){
    return this.http.post(`${this.API}update`,{
      id,cantidad
    });
  }
}
