import { NgModule } from '@angular/core';
import { PreloadAllModules, RouterModule, Routes } from '@angular/router';

const routes: Routes = [
  {
    path: 'home',
    loadChildren: () => import('./home/home.module').then( m => m.HomePageModule)
  },
  {
    path: '',
    redirectTo: 'carniceria',
    pathMatch: 'full'
  },
  {
    path: 'carniceria',
    loadChildren: () => import('./carniceria/carniceria.module').then( m => m.CarniceriaPageModule)
  },
  {
    path: 'insert-carne',
    loadChildren: () => import('./insert-carne/insert-carne.module').then( m => m.InsertCarnePageModule)
  },
  {
    path: 'edit-carne',
    loadChildren: () => import('./edit-carne/edit-carne.module').then( m => m.EditCarnePageModule)
  },
  {
    path: 'edit-carne/:id',
    loadChildren: () => import('./edit-carne/edit-carne.module').then( m => m.EditCarnePageModule)
  },
  {
    path: 'contact',
    loadChildren: () => import('./contact/contact.module').then( m => m.ContactPageModule)
  },
];

@NgModule({
  imports: [
    RouterModule.forRoot(routes, { preloadingStrategy: PreloadAllModules })
  ],
  exports: [RouterModule]
})
export class AppRoutingModule { }
