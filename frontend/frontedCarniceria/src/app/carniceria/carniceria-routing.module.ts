import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { CarniceriaPage } from './carniceria.page';

const routes: Routes = [
  {
    path: '',
    component: CarniceriaPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class CarniceriaPageRoutingModule {}
