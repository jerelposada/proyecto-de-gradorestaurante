import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { CarniceriaPageRoutingModule } from './carniceria-routing.module';

import { CarniceriaPage } from './carniceria.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    CarniceriaPageRoutingModule
  ],
  declarations: [CarniceriaPage]
})
export class CarniceriaPageModule {}
