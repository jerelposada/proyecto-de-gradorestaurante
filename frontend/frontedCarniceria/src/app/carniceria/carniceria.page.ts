import { Component, OnInit } from '@angular/core';
import { CarniceriaService } from '../comunicacion/carniceria.service';
@Component({
  selector: 'app-carniceria',
  templateUrl: './carniceria.page.html',
  styleUrls: ['./carniceria.page.scss'],
})
export class CarniceriaPage implements OnInit {

  Carnes: any = []
  constructor(private carniceriaService: CarniceriaService) { }

  ngOnInit() {
    this.carniceriaService.getAllCarniceria().subscribe(
      (res) => this.Carnes = res,
      (err) => console.log(err)
    );
  }

  ionViewDidEnter() {
    this.carniceriaService.getAllCarniceria().subscribe(
      (res) => this.Carnes = res,
      (err) => console.log(err)
    );
  }

}
