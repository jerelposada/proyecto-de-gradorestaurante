import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { EditCarnePageRoutingModule } from './edit-carne-routing.module';

import { EditCarnePage } from './edit-carne.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    EditCarnePageRoutingModule
  ],
  declarations: [EditCarnePage]
})
export class EditCarnePageModule {}
