import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { EditCarnePage } from './edit-carne.page';

const routes: Routes = [
  {
    path: '',
    component: EditCarnePage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class EditCarnePageRoutingModule {}
