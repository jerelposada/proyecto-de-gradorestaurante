import { Component, OnInit } from '@angular/core';
import { CarniceriaService } from '../comunicacion/carniceria.service';
import {Router,ActivatedRoute} from '@angular/router';

@Component({
  selector: 'app-edit-carne',
  templateUrl: './edit-carne.page.html',
  styleUrls: ['./edit-carne.page.scss'],
})
export class EditCarnePage implements OnInit {
  nombre: string;
  precio: number;
  Cantidad: number;
  Carness: any = {
      id: '',
      nombre: "",
      precio: "",
      unidadMedida: "",
      cantidad: "",
  }
  constructor(private carniceriaService:CarniceriaService,
              private router:Router,
              private  activatedRoute : ActivatedRoute)  { }

  ngOnInit() {
    this.activatedRoute.paramMap.subscribe((paramMap)=>{
      const id = paramMap.get('id');
      this.carniceriaService.getCarneByID(id).
      subscribe((res) => {
        this.Carness = res;
          this.nombre = this.Carness.res.nombre;
          this.precio = this.Carness.res.precio;
          this.Cantidad= this.Carness.res.cantidad;
        },
          err => console.log(err));
    });
  }

  updateCantidad(cantidad){
    const id =  this.Carness.res.id.toString()
    const _cantidad = cantidad.value;
      this.carniceriaService.UpdateCarneById(id,_cantidad).subscribe((res) => {
        alert("cantidad actualizada correctamente");
        this.router.navigate(["/carniceria"]);
      },
        err => console.log(err));
  }



}
