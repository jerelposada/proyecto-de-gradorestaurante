import { Component, OnInit } from '@angular/core';
import { CarniceriaService } from '../comunicacion/carniceria.service';

@Component({
  selector: 'app-insert-carne',
  templateUrl: './insert-carne.page.html',
  styleUrls: ['./insert-carne.page.scss'],
})
export class InsertCarnePage implements OnInit {

  constructor(private carniceriaService: CarniceriaService)  { }

  ngOnInit() {
  }
   saveCarne(nameCarne,cantidad,tipoCarne,unidadMedida,precioCarne){
     this.carniceriaService.saveCarne(nameCarne.value,precioCarne.value,unidadMedida.value,cantidad.value,tipoCarne.value).subscribe(
       (res) => console.log(res),
       (err) => console.log(err)
     );

  }
}
