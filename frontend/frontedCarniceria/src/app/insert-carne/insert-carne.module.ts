import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { InsertCarnePageRoutingModule } from './insert-carne-routing.module';

import { InsertCarnePage } from './insert-carne.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    InsertCarnePageRoutingModule
  ],
  declarations: [InsertCarnePage]
})
export class InsertCarnePageModule {}
