import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { InsertCarnePage } from './insert-carne.page';

const routes: Routes = [
  {
    path: '',
    component: InsertCarnePage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class InsertCarnePageRoutingModule {}
