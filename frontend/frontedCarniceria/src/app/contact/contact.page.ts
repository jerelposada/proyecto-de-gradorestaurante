import { Component, OnInit } from '@angular/core';
import { Contacts, Contact, ContactField, ContactName } from '@ionic-native/contacts/ngx';
import { Geolocation } from '@ionic-native/geolocation/ngx';


@Component({
  selector: 'app-contact',
  templateUrl: './contact.page.html',
  styleUrls: ['./contact.page.scss'],
})
export class ContactPage implements OnInit {

  latitud: number;
  longitud: number;
  constructor(private contacts: Contacts, public geolocation: Geolocation){ }

  getGeolocation(){
    this.geolocation.getCurrentPosition().then((resp) => {
      this.latitud = resp.coords.latitude;
      this.longitud =  resp.coords.longitude;
      console.log(this.latitud);
      console.log(this.longitud);
    }).catch((error) => {
      console.log('Error getting location', error);
    });
  }


  ngOnInit() {

  }
  createContact(){
    let contact: Contact = this.contacts.create();
    contact.name = new ContactName(null, 'juan', 'pedri');
    contact.phoneNumbers = [new ContactField('mobile', '6471234567')];
    contact.save().then(
      () => console.log('Contact saved!', contact),
      (error: any) => console.error('Error saving contact.', error)
    );
  }

  ionViewDidEnter(){
    this.getGeolocation();
    this.createContact();

  }
}
