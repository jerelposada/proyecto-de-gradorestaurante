const express = require('express');
const router = express.Router();
const  ModelCarniceria = require('../models/carniceria');
const typeCarniceria = require('../models/tipoCarniceria');

module.exports = function() {


    router.get('/', async function (req, res) {
        const Carnes = await ModelCarniceria.findAll({
            include:{
                model: typeCarniceria,
                attributes:['nombre']
            },
            attributes:['id','nombre','precio','unidadMedida','cantidad']
        });
        res.status(200).json(Carnes);
    })

    // inserta una carne
    router.post('/www', async function (req, res) {
        console.log(req.body)
        const { cantidad, _nombre } = req.body;

        const Carne = await ModelCarniceria.findOne({
            where:{
                nombre:_nombre
            },
        })
        res.status(200).json({res: Carne })

    })

    return router;
}