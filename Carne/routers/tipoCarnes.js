const express = require('express');
const router = express.Router();
const typeCarniceria = require('../models/tipoCarniceria');

module.exports = function() {

    //trae el listado de carnes
    router.get('/', async function (req, res) {
        const tipoCarniceria = await typeCarniceria.findAll();
        res.status(200).json(tipoCarniceria);
    })

    // inserta una carne
    router.post('/', async function (req, res) {
        const { nombre } = req.body;
        await typeCarniceria.create({
            nombre
        }).then(
            res.status(200).json({res: 'tipo de carne insertada correctamente' }))
    })

    return router;
}