const express = require('express');
const router = express.Router();
const  ModelCarniceria = require('../models/carniceria');
const typeCarniceria = require('../models/tipoCarniceria');

module.exports = function() {

    //trae el listado de carnes
    router.get('/', async function (req, res) {
        const Carnes = await ModelCarniceria.findAll({
            include:{
                model: typeCarniceria,
                attributes:['nombre']
            },
            attributes:['id','nombre','precio','unidadMedida','cantidad']
        });
        res.status(200).json(Carnes);
    })

    // inserta una carne
    router.post('/', async function (req, res) {
        console.log(req.body)
        const { nombre,precio,unidadMedida,cantidad ,TipoCarniceriaId } = req.body;
        await ModelCarniceria.create({
           nombre,precio,unidadMedida,cantidad ,TipoCarniceriaId
       }).then(
           res.status(200).json({res: 'carne insertada correctamente' }))
    })

   
    router.get('/:id', async function (req, res) {
        let _id = req.params.id;
       
        const Carne = await ModelCarniceria.findOne({
            include:{
                model: typeCarniceria,
                attributes:['nombre']
            },
            attributes:['id','nombre','precio','unidadMedida','cantidad'],
            where:{
                id: _id
            },
        })
        res.status(200).json({res: Carne })
    })

     
    router.post('/update', async function (req, res) {
        const {cantidad,id } = req.body;

        console.log("el valor de las propiedades es : ",req.body);
        
       const carne =  await ModelCarniceria.update({
            cantidad: cantidad
        }, {where: {id: id}})
        res.status(200).json({res: carne })
    })
    


    return router;
}