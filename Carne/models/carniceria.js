const Sequelize =  require('sequelize');
const db = require('../config/config');


const Carniceria = db.define('carniceria', {
 id:{
     type: Sequelize.INTEGER,
     primaryKey: true,
     autoIncrement: true,
 },
  nombre:{
     type:Sequelize.STRING(100),
  },
   precio:{
        type:Sequelize.INTEGER,
    },
    unidadMedida:{
        type:Sequelize.STRING(100),
    },
    cantidad:{
        type:Sequelize.INTEGER,
    }
});

module.exports = Carniceria;