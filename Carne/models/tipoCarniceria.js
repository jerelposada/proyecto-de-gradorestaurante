const Sequelize =  require('sequelize');
const db = require('../config/config');



const tipoCarniceria = db.define('tipoCarniceria',{
    id :{
        type: Sequelize.INTEGER,
        primaryKey: true,
        autoIncrement: true
    },
    nombre :{type: Sequelize.STRING(100)},
});

module.exports =tipoCarniceria;