const express = require('express')
const routesCarniceria = require('./routers/carniceria');
const typeCarniceria = require('./routers/tipoCarnes');
const compraVenta = require('./routers/comprarCarne');
const cors = require('cors');
const app = express()
app.use(cors());
const port = 3005


// crear la conexion a la bd
const db = require('./config/config');

db.sync({force:false})
    .then(() => console.log('Conectado al servidor'))
    .catch( (error) =>console.log(error))


require('./models/index');


app.use(express.urlencoded({ extended: false }));
app.use(express.json());

app.use('/tipoCarniceria',typeCarniceria());
app.use('/compraVenta',compraVenta());
app.use('/', routesCarniceria());




app.listen(port, () => {
    console.log(`Example app listening at http://localhost:${port}`)
})